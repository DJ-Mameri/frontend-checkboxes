import React, {Component} from 'react';

const nbItems = 4;
class Checkbox extends Component {

  constructor(props) {
    super(props);
    let listItems = [];
    for (let i = 1; i <= nbItems; i++) {
        listItems.push(
          {
            id: i,
            name: "item " + i,
            isChecked: false
          }
        );
    }
    this.state = {
      allChecked: false,
      listItems: listItems
    };
  }

  /**
   * 
   * @param {SyntheticBaseEvent} e 
   * @return {listItems, allChecked}
   */

  handleChange = (e) => {
    let itemName = e.target.name;
    let checked = e.target.checked;
    this.setState(prevState => {
      let { listItems, allChecked } = prevState;
      /** if Select all is checked, then checks all items */
      if (itemName === "selectAll") {
        allChecked = checked;
        listItems = listItems.map(item => ({ ...item, isChecked: checked }));
      } else {
        listItems = listItems.map(item =>
          item.name === itemName ? { ...item, isChecked: checked } : item
        );
        /** update allChecked variable: if all items are checkd, then allChecked = true */
        allChecked = listItems.every(item => item.isChecked);
      }
      return { listItems, allChecked };
    });
  };

  renderListItems = () => {
    return this.state.listItems.map(item => (
      <div>
        <input
          key={item.id}
          type="checkbox"
          name={item.name}
          value={item.name}
          checked={item.isChecked}
          onChange={this.handleChange}
        />
        <label>{item.name}</label>
      </div>
    ));
  };
  render() {
    return (
      <div>
        <input
          type="checkbox"
          name="selectAll"
          checked={this.state.allChecked}
          onChange={this.handleChange}
        />
        Select all
        <br />
        {this.renderListItems()}
      </div>
    );
  }
}

export default Checkbox;
